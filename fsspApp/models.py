from django.db import models
from django.contrib.auth.models import User
import datetime
import django.utils.timezone
from django.db.models.signals import post_save
from django.dispatch import receiver

from djFssp import settings


def get_first_name(self):
    return '%s %s' % (self.last_name,self.first_name)

User.add_to_class("__str__", get_first_name)

def debtorsAutoIncr():
    no = Debtors.objects.count()
    return no+1

# Create your models here.


class BlobField(models.Field):
    description = "Blob"

    def db_type(self, connection):
        return 'blob'

class KspList(models.Model):
    id = models.CharField(primary_key=True,max_length=5,null=False)
    name_ksp = models.CharField(max_length=100,null=True)
    def __str__(self):
        return self.name_ksp

class Ra(models.Model):
    id = models.IntegerField(primary_key=True,unique=True,null=False)
    name_ra =  models.CharField(max_length=100,null=True)
    def __str__(self):
        return self.name_ra

class Razdel(models.Model):
    id = models.CharField(max_length=6,primary_key=True,unique=True)
    name_razdel = models.CharField(max_length=100)
    def __str__(self):
        return self.name_razdel

class DocType(models.Model):
    id = models.CharField(primary_key=True,max_length=31,null=False)
    name_doc = models.CharField(max_length=50)
    def __str__(self):
        return self.name_doc

class ReasonReturn(models.Model):
    id = models.IntegerField(primary_key=True,null=False,unique=True)
    nameReason = models.CharField(max_length=200,null=False)
    def __str__(self):
        return str(self.id)+' - '+self.nameReason

class DebtorsPackage(models.Model):
    ID = models.AutoField(primary_key=True)
    TS = models.DateTimeField(null=True, verbose_name='Время загрузки')
    #PROTOCOL = BlobField(null=False, verbose_name='xml-файл с протоколом загрузки')
    TOTAL_FILES_COUNT = models.IntegerField(null=True, verbose_name='Всего обработано файлов')
    def __str__(self):
        return str(self.ID) + ') ' + str(self.TS)

class DebtorFile(models.Model):
    ID = models.AutoField(primary_key=True)
    FSSP_PACKAGE = models.ForeignKey(DebtorsPackage, on_delete=models.CASCADE, null=True, verbose_name='Ссылка на FSSP_PACKET')
    NAME = models.CharField(max_length=100, null=True, verbose_name='Имя файла (без расширения zip)')
    CONTENT = BlobField(null=False, verbose_name='Содержимое файла')
    SIZE = models.IntegerField(null=True)
    IDENT_STATUS = models.ForeignKey(ReasonReturn, null=True, blank=True, verbose_name='Статус идентификации')
    def __str__(self):
        return self.NAME

class Debtors(models.Model):
    ID = models.AutoField(primary_key=True, default=debtorsAutoIncr)
    #FILE_ID = models.IntegerField(null=True, unique=True,blank=True, verbose_name='Ссылка на FSSP_FILE')
    FSSP_FILE = models.OneToOneField(DebtorFile, on_delete=models.CASCADE, null=True, unique=True, blank=True, verbose_name='Файл ФССП')
    #PACKAGE_ID = models.IntegerField(null=True, unique=True, blank=True, verbose_name='Ссылка на FSSP_PACKAGE')
    FSSP_PACKAGE = models.ForeignKey(DebtorsPackage, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Пакет файлов ФССП')
    FSSP_ID = models.CharField(null=True, max_length=20,blank=True, verbose_name='ID документа ФССП')
    DOC_TYPE = models.ForeignKey(DocType, null=True,blank=True, verbose_name='Код вида документов')
    DOC_DATE = models.DateField(null=True,blank=True, verbose_name='Дата документа')
    #DEBTOR_TYPE = models.SmallIntegerField(null=True)
    DBTR_NAME = models.CharField(null=True, max_length=250,blank=True, verbose_name='ФИО должника от ФССП')
    DBTR_BORN = models.DateField(null=True,blank=True, verbose_name='Дата рождения')
    MAN_ID = models.CharField(null=True, max_length=30,blank=True, verbose_name='ID человека в ПТК НВП')
    NPERS = models.CharField(null=True, max_length=14,blank=True, verbose_name='СНИЛС из НВП')
    SNILS = models.CharField(null=True, max_length=14, blank=True, verbose_name='СНИЛС из файла ФССП')
    DBTR_BORN_ADR = models.CharField(null=True, max_length=250,blank=True, verbose_name='Место рождения')
    DBTR_ADR = models.CharField(null=True, max_length=250,blank=True, verbose_name='Адрес')
    #BARCODE = models.CharField(null=True, max_length=44)
    KSP = models.ForeignKey(KspList, null=True,blank=True, verbose_name='Код структурного подразделения')
    ADJUDICATION_TEXT = BlobField(null=True,blank=True, verbose_name='Установочная часть')
    RESOLUTION_TEXT = BlobField(null=True,blank=True, verbose_name='Постановочная часть')
    #FIO_SIGN = models.CharField(null=True, max_length=95)
    #SIGN_POST_CODE = models.CharField(null=True, max_length=25)
    #SIGN_POST = models.CharField(null=True, max_length=255)
    #IS_ORDER = models.IntegerField(null=True)
    #IS_SVOD = models.IntegerField(null=True)
    #COURT_ID = models.IntegerField(null=True)
    #COURT_NAME = models.CharField(null=True, max_length=1000)
    #SPI = models.CharField(null=True, max_length=95)
    IP_NO = models.CharField(null=True, max_length=25,blank=True, verbose_name='Номер ИП')
    IP_RISE_DATE = models.DateField(null=True,blank=True, verbose_name='Дата возбуждения ИП')
    #ID_DEBT_SUM = models.DecimalField(null=True, max_digits=15, decimal_places=2, verbose_name='УИН начисления')
    TOTAL_ARREST_DEBT_SUM = models.DecimalField(null=True, max_digits=15, decimal_places=2,blank=True, verbose_name='Полная сумма задолженности')
    IP_REST_DEBT_SUM = models.DecimalField(null=True, max_digits=15, decimal_places=2,blank=True, verbose_name='Задолженность')
    #ID_TYPE = models.IntegerField(null=True, verbose_name='УИН начисления')
    ID_DOC_NO = models.CharField(null=True, max_length=25,blank=True, verbose_name='Номер ИД')
    ID_DOC_DATE = models.DateField(null=True,blank=True, verbose_name='Дата ИД')
    #ID_DELO_NO = models.CharField(null=True, max_length=25)
    #ID_DELO_DATE = models.DateField(null=True)
    #ID_DEBT_CLS = models.IntegerField(null=True, )
    #ID_DEBT_TEXT = models.CharField(null=True, max_length=2000)
    ID_CRDR_NAME = models.CharField(null=True, max_length=1000,blank=True, verbose_name='Взыскатель')
    ID_CRDR_ADR = models.CharField(null=True, max_length=200,blank=True, verbose_name='Адрес взыскателя')
    IDENT_STATUS = models.ForeignKey(ReasonReturn,null=True,blank=True, verbose_name='Статус идентификации')
    RA = models.ForeignKey(Ra, null=True,blank=True, verbose_name='Район')
    RAZDEL = models.ForeignKey(Razdel, null=True, blank=True, verbose_name='Правовое основание выплаты')
    DATE_COMPLETED = models.DateTimeField(null=True, default=None, blank=True, verbose_name='Дата статуса')
    STATUS_EXECUTOR = models.ForeignKey(User, null=True, default=None, blank=True, verbose_name='Исполнитель')
    RECIPIENT_NAME = models.CharField(max_length=160,null=True,blank=True, verbose_name='Наименование получателя')
    RECIPIENT_OKTMO = models.CharField(max_length=11,null=True, blank=True, verbose_name='ОКТМО получателя')
    RECIPIENT_INN = models.CharField(max_length=12,null=True,blank=True, verbose_name='ИНН получателя')
    RECIPIENT_KPP = models.CharField(max_length=9,null=True,blank=True, verbose_name='КПП получателя')
    RECIPIENT_ACC_NUMBER = models.CharField(max_length=20,null=True,blank=True, verbose_name='Номер счета получателя')
    RECIPIENT_BANK_NAME = models.CharField(max_length=255,null=True,blank=True, verbose_name='Наименование банка получателя')
    RECIPIENT_BIC = models.CharField(max_length=9,null=True,blank=True, verbose_name='БИК банка получателя')
    AMOUNT = models.DecimalField(null=True, max_digits=15, decimal_places=2,blank=True, verbose_name='Перечисляемая сумма')
    FA = models.CharField(null=True, max_length=40, blank=True, verbose_name='Фамилия', db_index=True)
    IM = models.CharField(null=True, max_length=40, blank=True, verbose_name='Имя', db_index=True)
    OT = models.CharField(null=True, max_length=40, blank=True, verbose_name='Отчество', db_index=True)
    RDAT = models.DateField(null=True, blank=True, verbose_name='Дата рождения', db_index=True)
    LOAD_TS = models.DateTimeField(null=True, blank=True, verbose_name='Дата загрузки должника')
    IDENT_STATUS_COMMENT = models.CharField(max_length=600, null=True, blank=True, verbose_name='Комментарий изменения статуса идентификации')
    #UNIFO_CODE = models.CharField(max_length=25,null=True,blank=True, verbose_name='УИН начисления')
    def __str__(self):
        full_name = ''
        map_name = [self.FA,self.IM,self.OT]
        for map_item in map_name:
            if map_item is not None:
                full_name+=' '+map_item
        if len(full_name) == 0:
            full_name = str(self.ID)+' Инкогнито'
        return full_name

class UserPerm(User):
    class Meta:
        permissions = (
            # Идентификатор права       Описание права
            ("can_view_debtors", "Может работать с должниками района"),
            ("can_view_all_debtors", "Может работать со всеми должниками"),
            ("can_user_debtors_search", "Может использовать поиск должников"),
        )

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rayon = models.ForeignKey(Ra, default=0, null=True)
    middle_name = models.CharField(max_length=100,default=None,null=True)
    def __str__(self):
        return self.user.last_name+' '+self.user.first_name

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()