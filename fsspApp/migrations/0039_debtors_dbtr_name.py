# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-27 12:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fsspApp', '0038_auto_20171027_1104'),
    ]

    operations = [
        migrations.AddField(
            model_name='debtors',
            name='DBTR_NAME',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='ФИО должника от ФССП'),
        ),
    ]
