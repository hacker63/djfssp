# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-04 10:48
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fsspApp', '0025_auto_20171003_1033'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userperm',
            options={'permissions': (('can_view_debtors', 'Может работать с должниками района'), ('can_view_all_debtors', 'Может работать со всеми должниками'), ('can_user_debtors_search', 'Может использовать поиск должников'))},
        ),
    ]
