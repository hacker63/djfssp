# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-25 11:43
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fsspApp', '0014_auto_20170925_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='debtors',
            name='DATE_COMPLETED',
            field=models.DateTimeField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='debtors',
            name='STATUS_EXECUTOR',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
