# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-01 16:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fsspApp', '0045_auto_20171201_1635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='debtors',
            name='FSSP_FILE',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fsspApp.DebtorFile', verbose_name='Файл ФССП'),
        ),
    ]
