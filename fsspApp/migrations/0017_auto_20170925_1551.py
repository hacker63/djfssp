# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-25 11:51
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fsspApp', '0016_auto_20170925_1548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='debtors',
            name='ADJUDICATION_TEXT',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='BARCODE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='COURT_ID',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='COURT_NAME',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DATE_COMPLETED',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DBTR_ADR',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DBTR_BORN',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DBTR_BORN_ADR',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DBTR_NAME',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DEBTOR_TYPE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DOC_DATE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='DOC_TYPE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='FILE_ID',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='FIO_SIGN',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='FSSP_ID',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='IDENT_STATUS',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_CRDR_ADR',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_CRDR_NAME',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_DEBT_CLS',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_DEBT_SUM',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_DEBT_TEXT',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_DELO_DATE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_DELO_NO',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_DOC_DATE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_DOC_NO',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='ID_TYPE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='IP_NO',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='IP_REST_DEBT_SUM',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='IP_RISE_DATE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='IS_ORDER',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='IS_SVOD',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='KSP',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='MAN_ID',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='NPERS',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='RA',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='RAZDEL',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='RESOLUTION_TEXT',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='SIGN_POST',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='SIGN_POST_CODE',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='SNILS',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='SPI',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='STATUS_DEBTOR',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='STATUS_EXECUTOR',
        ),
        migrations.RemoveField(
            model_name='debtors',
            name='TOTAL_ARREST_DEBT_SUM',
        ),
    ]
