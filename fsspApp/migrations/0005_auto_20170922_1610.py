# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-22 12:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fsspApp', '0004_ksplist'),
    ]

    operations = [
        migrations.AddField(
            model_name='ksplist',
            name='id_ksp',
            field=models.CharField(max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='ksplist',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='ksplist',
            name='name_ksp',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
