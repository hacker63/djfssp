from django.contrib import admin
from .models import Debtors, KspList, Ra, Razdel, DocType, Profile, DebtorsPackage, DebtorFile

# Register your models here.
admin.site.register(Debtors)
admin.site.register(KspList)
admin.site.register(Ra)
admin.site.register(Razdel)
admin.site.register(DocType)
admin.site.register(Profile)

admin.site.register(DebtorsPackage)
admin.site.register(DebtorFile)