import io

from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Permission, User
from django.contrib.auth.decorators import login_required, permission_required
from django.views.defaults import permission_denied
from docxtpl import DocxTemplate

from .models import Debtors, DebtorFile
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from fsspApp import forms
import datetime
import re
from django.utils.encoding import iri_to_uri

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

# Create your views here.

def addUserView(request):
    context = {'title':'Страница добавления пользователя'}
    addUserForm = forms.addUserForm()
    if('POST' in request.method):
        addUserForm = forms.addUserForm(data=request.POST)
        if(addUserForm.is_valid()):
            userData = addUserForm.data
            user = User.objects.create_user(
                username=userData['username'].strip(),
                password=userData['password'].strip(),
                last_name=userData['last_name'],
                first_name=userData['first_name']
            )
            return render(request, 'success.html', context)
        else:
            print('INVALID')
    context['addUserForm'] = addUserForm
    return render(request, 'add-user.html', context)

def download_doc_report(request, id_debtor):
    debtor = Debtors.objects.get(ID=id_debtor)
    context = {'FIO': debtor,
               'DBTR_BORN': debtor.DBTR_BORN,
               'SNILS': debtor.SNILS,
               'RA': debtor.RA,
               'DBTR_ADR': debtor.DBTR_ADR,
               'DBTR_BORN_ADR': debtor.DBTR_BORN_ADR,
               'DOC_TYPE': debtor.DOC_TYPE,
               'ADJUDICATION_TEXT': debtor.ADJUDICATION_TEXT.decode(),
               'RESOLUTION_TEXT': debtor.RESOLUTION_TEXT.decode(),
               'ID_DOC_NO': debtor.ID_DOC_NO,
               'TOTAL_ARREST_DEBT_SUM': debtor.TOTAL_ARREST_DEBT_SUM,
               'IP_REST_DEBT_SUM': debtor.IP_REST_DEBT_SUM,
               'ID_CRDR_NAME': debtor.ID_CRDR_NAME,
               'ID_CRDR_ADR': debtor.ID_CRDR_ADR
               }
    if (debtor.DOC_TYPE.id == 'O_IP_ACT_DBT_CANCEL'):
        doc = DocxTemplate("docNotRecipient.docx")
    else:
        context['RECIPIENT_NAME'] = debtor.RECIPIENT_NAME
        context['RECIPIENT_OKTMO'] = debtor.RECIPIENT_OKTMO
        context['RECIPIENT_INN'] = debtor.RECIPIENT_INN
        context['RECIPIENT_KPP'] = debtor.RECIPIENT_KPP
        context['RECIPIENT_ACC_NUMBER'] = debtor.RECIPIENT_ACC_NUMBER
        context['RECIPIENT_BANK_NAME'] = debtor.RECIPIENT_BANK_NAME
        context['RECIPIENT_BIC'] = debtor.RECIPIENT_BIC
        context['AMOUNT '] = debtor.AMOUNT
        doc = DocxTemplate("docRecipient.docx")
    doc.render(context)
    obj = io.BytesIO()
    doc.save(obj)
    if(debtor.FA and debtor.IM and debtor.OT):
        FIO = str(debtor.FA).lower()
        FIO = FIO[0].upper() + FIO[1:] + debtor.IM[0].upper() + debtor.OT[0].upper()
        nameFileReport = FIO + '_' + debtor.NPERS
    else:
        nameFileReport = debtor.DBTR_NAME
    nameFileReport = nameFileReport.strip()
    response = HttpResponse(obj.getvalue(),
                            content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    response['Content-Disposition'] = u'attachment; filename=%s.docx' % iri_to_uri(nameFileReport)
    return response


def generate_package_report(request, id_package):
    """
    Создаем XML файл.
    """
    fileList = DebtorFile.objects.filter(FSSP_PACKAGE=id_package)
    if (Debtors.objects
            .filter(FSSP_PACKAGE=id_package)
            .filter(IDENT_STATUS=0)
            .count() > 0):
        return HttpResponse('Невозможно сформировать отчёт, имеются должники со статусом "Не в работе"')
    root = ET.Element("PFR_SED_FSSP")
    DocDate = ET.SubElement(root, 'DocDate')
    DocDate.text = datetime.datetime.now().strftime("%d.%m.%Y %H:%M")
    FSSPFiles = ET.Element('FSSPFiles')
    for file in fileList:
        obj = ET.Element('FSSPFile')
        obj_FileName = ET.SubElement(obj, 'FileName')
        obj_FileName.text = str(file.NAME)
        obj_Status = ET.SubElement(obj, 'Status')
        obj_Status.text = str(file.IDENT_STATUS)
        try:
            debtor = Debtors.objects.get(FSSP_FILE=file.ID)
            debtorObj = ET.Element('Debtor')
            debtorObj_id = ET.SubElement(debtorObj, 'ID')
            debtorObj_id.text = str(debtor.ID)
            debtorObj_id = ET.SubElement(debtorObj, 'FSSP_ID')
            debtorObj_id.text = str(debtor.FSSP_ID)
            debtorObj_id = ET.SubElement(debtorObj, 'DBTR_NAME')
            debtorObj_id.text = str(debtor.DBTR_NAME)
            debtorObj_id = ET.SubElement(debtorObj, 'IDENT_STATUS')
            debtorObj_id.text = str(debtor.IDENT_STATUS)
            obj.append(debtorObj)
        except Debtors.DoesNotExist:
            pass
        FSSPFiles.append(obj)
    root.append(FSSPFiles)
    obj = io.BytesIO(ET.tostring(root))
    response = HttpResponse(obj, content_type='application/xml')
    nameFileReport = str(id_package) + '_' + datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    response['Content-Disposition'] = "attachment; filename=" + nameFileReport + ".xml"
    return response

def downloadFsspFile(request, file_id):
    obj = DebtorFile.objects.get(ID=file_id)
    response = HttpResponse(obj.CONTENT, content_type='application/octet-stream')
    nameFileReport = '123'
    response['Content-Disposition'] = "attachment; filename=" + nameFileReport + ".rar"
    return response
