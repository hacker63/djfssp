from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^download/doc-report/(?P<id_debtor>[0-9]+)', views.download_doc_report, name='download_doc_report'),
    url(r'^packages/(?P<id_package>[0-9]+)/generate-report', views.generate_package_report, name='generate_package_report'),
    url(r'^files/(?P<file_id>[0-9]+)', views.downloadFsspFile, name='download_fssp_file')
]
