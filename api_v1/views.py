import datetime

from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import Http404
from django.shortcuts import render
from rest_framework.pagination import PageNumberPagination

from djFssp.paginators import CustomPagination
from fsspApp import models as fsspModels
from rest_framework import generics, viewsets

from api_v1.serializers import myProfileSerializer, \
    debtorsSerializer,\
    packagesListSerializer, \
    debtorSerializer, \
    identStatusSerializer, \
    raSerializer, \
    fsspFileListSerializer

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

class myProfileView(APIView):
    serializer = myProfileSerializer
    def get(self, request):
        object = request.user
        serObjects = myProfileSerializer(object)
        return Response(serObjects.data)

class debtorsListView(APIView):
    def get(self, request, id):
        debtorList = fsspModels.Debtors.objects.filter(FSSP_PACKAGE=id).order_by('-ID')
        paginator = CustomPagination()
        page = paginator.paginate_queryset(debtorList, request)
        serializer = debtorsSerializer(page, many=True, context={'request': request})
        return paginator.get_paginated_response(serializer.data)
    def post(self, request, id):
        dictSearch = {}
        print(request.data)
        for key_dict, value_dict in request.data.items():
            if(value_dict is not None and key_dict != 'page'):
                if (key_dict == 'FA' or key_dict == 'IM' or key_dict == 'OT'):
                    dictSearch[key_dict + '__icontains'] = value_dict
                else:
                    dictSearch[key_dict] = value_dict
        print(dictSearch)
        if('SEARCH_OUT' in request.data):
            dictSearch.pop('SEARCH_OUT')
            if(request.data['SEARCH_OUT'] == True):
                debtorList = fsspModels.Debtors.objects.filter(**dictSearch)
            else:
                debtorList = fsspModels.Debtors.objects.filter(
                    FSSP_PACKAGE=fsspModels.DebtorsPackage.objects.get(ID=id)).filter(**dictSearch)
        else:
            debtorList = fsspModels.Debtors.objects.filter(FSSP_PACKAGE=fsspModels.DebtorsPackage.objects.get(ID=id))\
                .filter(**dictSearch).order_by('-ID')
        paginator = CustomPagination()
        page = paginator.paginate_queryset(debtorList, request)
        serializer = debtorsSerializer(page, many=True, context={'request': request})
        return paginator.get_paginated_response(serializer.data)

class packagesListView(APIView):
    def get(self, request):
        packagesList = fsspModels.DebtorsPackage.objects.order_by('-TS')
        paginator = PageNumberPagination()
        page = paginator.paginate_queryset(packagesList, request)
        serializer = packagesListSerializer(page, many=True, context={'request': request})
        result = serializer.data
        for i in range(0,len(result)):
            if(fsspModels.Debtors.objects.filter(FSSP_PACKAGE=result[i]['ID']).filter(IDENT_STATUS=0).count()):
                result[i]['canGenerateReport'] = False
            else:
                result[i]['canGenerateReport'] = True
        return paginator.get_paginated_response(result)

class fsspFileListView(APIView):
    def get(self, request, package_id):
        fileList = fsspModels.DebtorFile.objects.filter(FSSP_PACKAGE=package_id)
        paginator = CustomPagination()
        page = paginator.paginate_queryset(fileList, request)
        serializer = fsspFileListSerializer(page, many=True, context={'request': request})
        result = serializer.data
        for file in result:
            try:
                debtor = fsspModels.Debtors.objects.get(FSSP_FILE=file['ID'])
                file['DEBTOR'] = debtorSerializer(debtor).data
            except fsspModels.Debtors.DoesNotExist:
                file['DEBTOR'] = None
        return paginator.get_paginated_response(result)

class getPackageView(APIView):
    def get(self, request, id):
        package = fsspModels.DebtorsPackage.objects.get(ID=id)
        context = {}
        context['ID'] = package.ID
        context['TS'] = package.TS
        context['TOTAL_FILES_COUNT'] = package.TOTAL_FILES_COUNT
        return Response(context)

class debtorView(APIView):
    def get_object(self, id):
        try:
            return fsspModels.Debtors.objects.get(ID=id)
        except fsspModels.Debtors.DoesNotExist:
            raise Http404
    def get(self, request, id):
        debtorObj = self.get_object(id)
        serializer = debtorSerializer(debtorObj)
        resultData = serializer.data
        return Response(resultData)

class debtorToCompleteView(APIView):
    def get(self, request, id):
        debtorObj = fsspModels.Debtors.objects.get(ID=id)
        if debtorObj.STATUS_EXECUTOR and debtorObj.STATUS_EXECUTOR.id != request.user.id:
            if(debtorObj.STATUS_EXECUTOR != request.user):
                return Response({'error':'Должник уже взят в обработку другим специалистом'},400)
            debtorObj.DATE_COMPLETED = datetime.datetime.now()
            debtorObj.STATUS_EXECUTOR = request.user
            debtorObj.save()
            serializer = debtorSerializer(debtorObj)
            resultData = serializer.data
            return Response(resultData)
        else:
            return Response('error', 400)

class debtorToProcessView(APIView):
    def get(self, request, id):
        debtorObj = fsspModels.Debtors.objects.get(ID=id)
        if(debtorObj.STATUS_EXECUTOR is None):
            debtorObj.DATE_COMPLETED = datetime.datetime.now()
            debtorObj.STATUS_EXECUTOR = request.user
            debtorObj.save()
            serializer = debtorSerializer(debtorObj)
            resultData = serializer.data
            return Response(resultData)
        else:
            return Response({'error': 'Должник уже был взят в обработку другим специалистом'}, 400)

class debtorToCancelView(APIView):
    def get(self, request, id):
        debtorObj = fsspModels.Debtors.objects.get(ID=id)
        if debtorObj.STATUS_EXECUTOR and debtorObj.STATUS_EXECUTOR.id == request.user.id:
            if (debtorObj.STATUS_EXECUTOR and debtorObj.STATUS_EXECUTOR != request.user):
                return Response({'error': 'Должник взят в обработку другим специалистом'}, 400)
            debtorObj.DATE_COMPLETED = datetime.datetime.now()
            debtorObj.STATUS_EXECUTOR = None
            debtorObj.IDENT_STATUS_COMMENT = None
            debtorObj.save()
            serializer = debtorSerializer(debtorObj)
            resultData = serializer.data
            return Response(resultData)
        else:
            return Response({'error': 'Должник не в работе'}, 400)

class getIdentStatusListView(APIView):
    def get(self, request):
        iStatusList = fsspModels.ReasonReturn.objects.all()
        serializer = identStatusSerializer(iStatusList, many=True)
        return Response(serializer.data)

class setIdentStatusView(APIView):
    def post(self, request, id_debtor):
        requestData = request.data
        debtorObj = fsspModels.Debtors.objects.get(ID=id_debtor)
        if(debtorObj.STATUS_EXECUTOR and request.user != debtorObj.STATUS_EXECUTOR and debtorObj.STATUS_DEBTOR.short_num_status == 2):
            return Response({'error': 'Должник находится в обработке у другого специалиста.'})
        print(request.data)
        debtorObj.IDENT_STATUS = fsspModels.ReasonReturn.objects.get(id=requestData['IDENT_STATUS'])
        debtorObj.IDENT_STATUS_COMMENT = requestData['IDENT_STATUS_COMMENT']
        debtorObj.save()
        serializer = debtorSerializer(debtorObj)
        resultData = serializer.data
        return Response(resultData)

class raListView(APIView):
    def get(self, request):
        raList = fsspModels.Ra.objects.all()
        serializer = raSerializer(raList, many=True)
        return Response(serializer.data)