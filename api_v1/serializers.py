from django.contrib.auth.models import User
from rest_framework import serializers
from fsspApp import models as fsspModels

class myProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username','last_name','first_name','email']

class razdelDebtorSerializer(serializers.ModelSerializer):
    class Meta:
        model = fsspModels.Razdel
        fields = '__all__'

class statusExecutorSerializr(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name']

class raSerializer(serializers.ModelSerializer):
    class Meta:
        model = fsspModels.Ra
        fields = '__all__'

class identStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = fsspModels.ReasonReturn
        fields = '__all__'

class docTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = fsspModels.DocType
        fields = '__all__'

class debtorsSerializer(serializers.ModelSerializer):
    STATUS_EXECUTOR = statusExecutorSerializr(many=False)
    RA = raSerializer(many=False)
    RAZDEL = razdelDebtorSerializer(many=False)
    IDENT_STATUS = identStatusSerializer(many=False)
    DOC_TYPE = docTypeSerializer(many=False)
    class Meta:
        model = fsspModels.Debtors
        fields = '__all__'

class packagesListSerializer(serializers.ModelSerializer):
    class Meta:
        model = fsspModels.DebtorsPackage
        fields = '__all__'

class debtorSerializer(serializers.ModelSerializer):
    STATUS_EXECUTOR = statusExecutorSerializr(many=False)
    RA = raSerializer(many=False)
    RAZDEL = razdelDebtorSerializer(many=False)
    IDENT_STATUS = identStatusSerializer(many=False)
    DOC_TYPE = docTypeSerializer(many=False)
    class Meta:
        model = fsspModels.Debtors
        fields = '__all__'

class fsspFileListSerializer(serializers.ModelSerializer):
    IDENT_STATUS = identStatusSerializer(many=False)
    FSSP_PACKAGE = packagesListSerializer(many=False)
    class Meta:
        model = fsspModels.DebtorFile
        fields = ['ID', 'FSSP_PACKAGE', 'NAME', 'SIZE', 'IDENT_STATUS']