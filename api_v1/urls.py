from django.conf.urls import url, include
from api_v1 import views as apiViews

urlpatterns = [
    url(r'^my_profile', apiViews.myProfileView.as_view()),
    url(r'^rayons/', apiViews.raListView.as_view()),
    url(r'^packages/(?P<id>\d+)$', apiViews.debtorsListView.as_view()),
    url(r'^packages/(?P<package_id>\d+)/files', apiViews.fsspFileListView.as_view()),
    url(r'^packages/$', apiViews.packagesListView.as_view()),
    url(r'^package/(?P<id>[0-9]+)', apiViews.getPackageView.as_view()),
    url(r'^debtors/(?P<id>[0-9]+)$', apiViews.debtorView.as_view()),
    url(r'^debtors/(?P<id>[0-9]+)/toComplete', apiViews.debtorToCompleteView.as_view()),
    url(r'^debtors/(?P<id>[0-9]+)/toProcess', apiViews.debtorToProcessView.as_view()),
    url(r'^debtors/(?P<id>[0-9]+)/toCancel', apiViews.debtorToCancelView.as_view()),
    url(r'^debtors/(?P<id_debtor>[0-9]+)/edit-status-ident/', apiViews.setIdentStatusView.as_view()),
    url(r'^ident-status/list', apiViews.getIdentStatusListView.as_view()),
]
